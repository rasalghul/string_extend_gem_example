require 'rubygems'

spec = Gem::Specification.new do |s|
	s.name = 'string_extend'
	s.version = '0.0.1'
	s.summary = "StringExtends adds useful features to the String Class"
	s.files = Dir.glob("**/**/**")
	s.test_files = Dir.glob("test/*_test.rb")
    # deprecated autorequire
	s.autorequire = 'string_extend'
    # Needs a licence
    # Deprecated rdoc notation
	s.author = "Carlos Gomez"
	s.email = "carlosgomez.deb@gmail.com"
	s.has_rdoc = false
	s.required_ruby_version = '>= 1.8.2'
end	
